<?php get_header(); ?>

  <div class="container">
    <?php get_template_part('includes/pre-content'); ?>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <div class="slider">
        <div id="carousel" class="carousel slide"> <img src="<?php bloginfo('stylesheet_directory'); ?>/img/featured-band.png" class="featured-band">
          <ol class="carousel-indicators">
            <li data-target="#carousel" data-slide-to="0" class=""></li>
            <li data-target="#carousel" data-slide-to="1" class="active"></li>
            <li data-target="#carousel" data-slide-to="2" class=""></li>
          </ol>

          <div class="carousel-inner">
            <div class="item"> <img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider-image.png" alt="">
              <div class="carousel-caption">
                <h4 class="bold text-white">PREMIER <span class="text-orange">ENTERTAINMENT PROVIDER</span></h4>
                <p>Accompanied by Anna 'Blacky' Savage, our most recent addition to the writing team here at Crack In The Road, we found an ideal spot in the cavernous Manchester Academy 1 to witness the folk infused rock sound of Band of Horses. Known to many simply by their hit, yet morbidly named, single 'The Funeral', Band of Horses made sure that the sold out audience became very aware of their fantastic back catalogue of tracks. Support for the evening was provided by Crack In The Road favourites Goldheart Assembly, and Mojave 3.</p>
              </div>
            </div>
              
            <div class="item active"> <img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider-image.png" alt="">
              <div class="carousel-caption">
                <h4 class="bold text-white">PREMIER <span class="text-orange">ENTERTAINMENT PROVIDER</span></h4>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              </div>
            </div>
              
            <div class="item"> <img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider-image.png" alt="">
              <div class="carousel-caption">
                <h4 class="bold text-white">PREMIER <span class="text-orange">ENTERTAINMENT PROVIDER</span></h4>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              </div>
            </div>
          </div>
          <a class="left " href="#carousel" data-slide="prev"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/arrow-left.png"></a> <a class="right" href="#carousel" data-slide="next"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/arrow-right.png"></a> </div>
        </div>

        <?php the_content(); ?>
      </div>

      <hr class="no-margin">
      
      <div class="container">
        <div class="row">
          <div class="span7 featured">
            <?php echo do_shortcode('[smartblock id=69]'); ?>
          </div>

          <div class="span5 contact-testimonial-holder">
            <div class="latest-testimonial shadowed-box">
              <img src="<?php bloginfo('stylesheet_directory'); ?>/img/testimonials-icon.png" class="testimonial-icon">
              
              <h4 class="text-black bold">LATEST TESTIMONIAL</h4>
              <?php echo do_shortcode('[latest-testimonial]'); ?>

              <a href="/testimonials" class="button text-grey">More</a>
            </div>

            <div class="homepage-contact shadowed-box">
              <h4 class="text-black bold">SEND US AN EMAIL</h4>
              <?php echo do_shortcode('[contact-form-7 id="128" title="Quick Contact"]'); ?>
            </div>
        </div> <!-- end row -->
      </div> <!-- end container -->
    <?php endwhile; endif; ?>
  
  </div> <!-- end container -->

<?php get_footer(); ?>