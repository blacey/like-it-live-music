<?php
	//error_reporting(E_ALL);
	//ini_set('display_errors', '1');
	

	// Add theme image sizes
	add_image_size( 'band-thumbnail', '249px', '202px', true );           //  Band List pages
	add_image_size( 'band-slide', '537px', '339px', true );               //  Band Slides
	add_image_size( 'related-band-thumbnail', '147px', '147px', true );   //  At the bottom of the band profile page

	

	// Get Shortcodes
	get_template_part('shortcodes/contact-details');


	// Get the custom post types
	get_template_part('custom-post-types/bands');
	get_template_part('custom-post-types/slider');
	get_template_part('custom-post-types/testimonials');
	get_template_part('custom-post-types/featured-videos');


	if(!function_exists('load_scripts')){
		function load_scripts(){
			// Enqueue Bootstrap Scripts
			wp_register_script('bootstrap', get_stylesheet_directory_uri().'', array( 'jquery' ), null, false );
			wp_register_script('main-scripts', get_stylesheet_directory_uri().'/js/main.js', array( 'jquery' ), null, false );
		}
		add_action('wp_head', 'load_scripts');
	}


	
	// theme support
	add_theme_support( 'menus' );
	add_theme_support( 'post-thumbnails' );
	add_filter('widget_text', 'do_shortcode');    // Allow shortcodes to be run from widget areas
	
	
	// remove junk from head
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'index_rel_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'feed_links_extra',                3 );
	remove_action( 'wp_head', 'start_post_rel_link',             10, 0 );
	remove_action( 'wp_head', 'parent_post_rel_link',            10, 0 );
	remove_action( 'wp_head', 'adjacent_posts_rel_link',         10, 0 );
	remove_action( 'wp_head', 'wp_shortlink_wp_head',            10, 0 );
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	
	
	// allow id attribute in posts/pages
	foreach($allowedposttags as $k=>$v){
		$allowedposttags[$k]['id'] = array();
	}



	// Register Sidebars
	register_sidebar(array(
		'name' 			 => 'Pre Header',
		'id'            => "pre-header",
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => ''
	));

	register_sidebar(array(
		'name' 			 => 'Left Sidebar',
		'id'            => "sidebar-left",
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>'
	));


	register_sidebar(array(
		'name' 			 => 'Right Sidebar',
		'id'            => "sidebar-right",
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>'
	));
	
