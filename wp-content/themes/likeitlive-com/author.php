
<?php get_header(); ?>
	<div id="pre-content"><?php get_template_part('includes/pre-content'); ?></div>
			
	<div id="content">
		<div class="inner container_16">
			<div id="posts" class="grid_16 alpha">
				<?php $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); ?>
				
				<h2>About <?php if($curauth->first_name != ''){ echo $curauth->first_name . ' ' . $curauth->last_name; } else {  echo $curauth->nickname; } ?></h2>
				<strong>Website</strong>
				<p><a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a></p>
				
				<strong>Profile</strong>
				<p><?php echo $curauth->user_description; ?></p> 
				<hr/>
				
				<h2><?php if($curauth->first_name != ''){ echo $curauth->first_name . ' ' . $curauth->last_name; } else {  echo $curauth->nickname; } ?>'s posts</h2>
				<ul>
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<li>
							<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>">
							<?php the_title(); ?></a>,
							<?php the_time('d M Y'); ?> in <?php the_category('&');?>
						</li>
					<?php endwhile; else: ?>
						<p><?php _e('No posts by this author.'); ?></p>
					<?php endif; ?>
				</ul>
			</div>
		
		</div>
	</div>
	
	<div id="post-content"><?php get_template_part('includes/post-content'); ?></div>
	
<?php get_footer(); ?>