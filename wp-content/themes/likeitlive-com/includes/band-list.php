<div class="row">
	<div class="span4">

		<div class="band">
			<?php $custom_meta = get_post_custom(get_the_ID()); ?>
			
			<?php if(has_post_thumbnail()): ?>
				<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail('band-thumbnail'); ?>
				</a>
			<?php endif; ?>

			<h5><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h5>
			<?php if($custom_meta['bandratefrom'][0]): ?>
				<h6>FROM: <?php echo $custom_meta['bandratefrom'][0]; ?></h6>
			<?php endif; ?>

			<p>
				<?php
					if($custom_meta['bandprofile'][0]):
						echo substr($custom_meta['bandprofile'][0], 0, 250) . '...';
					endif;
				?>
			</p>
		</div> <!-- end band -->

	</div> <!-- end span4 -->
</div> <!-- end row -->