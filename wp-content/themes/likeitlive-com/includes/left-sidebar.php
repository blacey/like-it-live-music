<?php if ( is_active_sidebar( 'sidebar-left' ) ) : ?>
	<ul id="sidebar">
		<?php dynamic_sidebar( 'sidebar-left' ); ?>
	</ul>
<?php endif; ?>