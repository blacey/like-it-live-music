<div class="span2 pagination">
	<?php
      $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
		$range = 5;

      $pageNumber = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$pages = $wp_query->max_num_pages;
	?>

   	<ul>							
   		<?php if(get_query_var('paged') > 1): ?>
   			<li><a class="pagination-arrow" href="<?php echo get_bloginfo('siteurl') . '/' . $term->taxonomy . '/' . $term->slug . '/page/' . ($pageNumber-1) ?>">&lt;</a></li>
   		<?php endif; ?>

   		<?php
   			$i = 0;
            while ($i < $pages) : $i++;
            	if($pages > 1):
 	               if ($i == $pageNumber) {
    	              echo '<li class="current text-pink"><a href="#">'.$i.'</a></li>';
        	       } else {
            	      echo '<li><a href="' . get_bloginfo('siteurl') . '/' . $term->taxonomy . '/' . $term->slug . '/page/'.$i.'/">'.$i.'</a></li>';
               	   }
               	endif;
            endwhile;
         ?>

         <?php if($pageNumber < $i): ?>
         	<li><a class="pagination-arrow" href="<?php echo get_bloginfo('siteurl') . '/' . $term->taxonomy . '/' . $term->slug . '/page/' . ($pageNumber+1) ?>">&gt;</a></li>
      	<?php endif; ?>
      </ul>
   
</div> <!-- end pagination -->