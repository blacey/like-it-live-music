<div class="row socials-slogan">
	<h1 class="main-slogan span6">
		<?php
			if(is_category()){
				single_cat_title();				
			} elseif(is_tax()){
				$term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
				echo $term->name;
			} elseif(is_404()) { 
				echo 'Page Not Found';
			} elseif(is_page() || is_single()){
				the_title();
			} else {
				echo 'LikeItLive Bands - <span>Live Music</span>';
			}
		?>
	</h1>
   
   <div class="span2 offset1 socials top-socials">
     <?php echo do_shortcode('[smartblock id=11]'); ?>
   </div>
</div>

<?php if(get_post_type() == "band" && !is_tax()): ?>
	<?php $custom_meta = get_post_custom(get_the_ID()); ?>
	
	
  <!--
    <div class="slider">
      <div id="carousel" class="carousel slide has-buttons">
        <ol class="carousel-indicators">
          <li data-target="#carousel" data-slide-to="0" class=""></li>
          <li data-target="#carousel" data-slide-to="1" class="active"></li>
          <li data-target="#carousel" data-slide-to="2" class=""></li>
        </ol>

        <div class="carousel-inner">
          <div class="item active"> <img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider-image.png" alt="">
            <div class="carousel-caption">
              <p>We offer wedding bands and music for wedding functions throughout Gloucestershire including Berkeley, Blakeney, Cheltenham, Chipping Campden, Cinderford, Cirencester, Coleford, Drybrook, Dursley, Dymock, and many more.</p>
              <div class="band-buttons">
              		<a href="#" class="book-band">Book this band</a>
              		<a href="#" class="button large">Make Enquiry</a>
              	</div>
              </div>
          </div>
          <div class="item"> <img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider-image.png" alt="">
            <div class="carousel-caption">
              <p>We offer wedding bands and music for wedding functions throughout Gloucestershire including Berkeley, Blakeney, Cheltenham, Chipping Campden, Cinderford, Cirencester, Coleford, Drybrook, Dursley, Dymock, and many more.</p>
              <div class="band-buttons">
              		<a href="#" class="book-band">Book this band</a>
              		<a href="#" class="button large">Make Enquiry</a>
              	</div>
              </div>
          </div>
          <div class="item"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/slider-image.png" alt="">
            <div class="carousel-caption">
              <p>We offer wedding bands and music for wedding functions throughout Gloucestershire including Berkeley, Blakeney, Cheltenham, Chipping Campden, Cinderford, Cirencester, Coleford, Drybrook, Dursley, Dymock, and many more.</p>
              <div class="band-buttons">
              		<a href="#" class="book-band">Book this band</a>
              		<a href="#" class="button large">Make Enquiry</a>
              	</div>
              </div>
          </div>
        </div>

        <a class="left " href="#carousel" data-slide="prev"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/arrow-left.png"></a>
        <a class="right" href="#carousel" data-slide="next"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/arrow-right.png"></a> </div>
    </div>
  -->

  <?php
    //echo 'Band Slider ID: ' . $custom_meta['bandslider'][0];
  
    $args = array(
      'post_type' => 'sliders',
      'tax_query' => array(
            array(
                'taxonomy'  => 'slider-category',
                'field'     => 'term_id',
                'terms'     => $custom_meta['bandslider'][0],
                'operator'  => 'IN'
            )
      ),
    );
    $query = new WP_Query($args);

    
  echo '<div class="slider">';
	echo '<div id="carousel" class="carousel slide has-buttons">';
	  echo '<div class="carousel-inner">';

		$i = 1;
		

			if($i == 1){
			  echo '<div class="item active">';
			} else {
			  echo '<div class="item">';
			}
		  	
		  	if($query->have_posts() && $custom_meta['bandslider'][0] != 0):
		  		while($query->have_posts()): $query->the_post();
		   		    $image = wp_get_attachment_image_src(get_the_ID());
					if(has_post_thumbnail()):
					  the_post_thumbnail(get_the_ID());
					endif;
		
					echo '<div class="carousel-caption">';
					  echo '<h2>' . get_the_title() . '</h2>';
					  the_content();
					  echo '<div class="band-buttons">';
					  echo '  <a href="/contact-us/" class="book-band">Book this band</a>';
					  echo '  <a href="#" class="button large">Make Enquiry</a>';
					  echo '</div>';
					echo '</div>';
				endwhile;
			else:
				echo '<img src="http://placehold.it/924x350" alt="Gallery Coming Soon" />';
			endif;
		  echo '</div>'; // end .item

		  $i++;
	  echo '</div>'; // end .carousel-inner
	echo '</div>'; // end #carousel
  echo '</div>'; // end .slider
?>

<div class="container">
<div class="row">
<div class="span6">
	<div class="social-media-buttons">
		<span class='st_twitter_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='twitter'></span><span class='st_facebook_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='facebook'></span><span class='st_plusone_hcount' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>' displayText='plusone'></span>
	</div>
</div>

<div class="span6">
	<?php if($custom_meta['bandratefrom'][0]): ?>
		<div class="cost-of-band goRight">
			<div class="inner">
				<?php if($custom_meta['bandratefrom'][0]): ?>
					From: <?php echo $custom_meta['bandratefrom'][0]; ?>
				<?php endif; ?>

				<?php if($custom_meta['bandrateto'][0]): ?>
					To: <?php echo $custom_meta['bandrateto'][0]; ?>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
</div>

<?php endif; ?>
