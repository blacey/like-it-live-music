
<?php 
	$currentband = get_the_ID();
	
	if(get_post_type() == "band" && !empty($currentband)): ?>
	
	<?php		
		$query = null;
		$args = array(
			'post_type' => $post_type, 
			"$tax" => $tax_term->slug, 
			'post_status' => 'published', 
			'showposts' => '6',
			'meta_key' => '_thumbnail_id',
			'post__not_in' => array($currentband)
		);
		$query = new WP_Query($args);
					
		if( $query->have_posts() ): ?>
		
			<div class="similar-acts">
				<div class="container">
					<h3>SIMILAR ACTS...</h3>
					<ul>		
						<?php while ($query->have_posts()) : $query->the_post(); ?>
							<?php if(has_post_thumbnail()): ?>
								<li>
									<a href="<?php the_permalink(); ?>">
										<?php  
											the_post_thumbnail('related-band-thumbnail');
										?>
									</a>
								</li>
							<?php endif; ?>
						<?php endwhile; /* end loop of bands for the current genre */ ?>
					<?php endif; /* end if have posts */ ?>
		   		</ul>
		 	</div>
		</div>
		
	<?php wp_reset_query(); ?> 

<?php endif; // end if post type ?>