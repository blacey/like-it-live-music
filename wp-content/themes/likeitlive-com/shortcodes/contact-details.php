<?php
	
	if(!function_exists('get_telephone')){
		function get_telephone(){
			return strip_tags(do_shortcode('smartblock id=62'));
		}
		add_shortcode('get-telephone', 'get_telephone');
	}
