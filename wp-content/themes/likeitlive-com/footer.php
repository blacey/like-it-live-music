		</div> <!-- end main -->
		
		<?php get_template_part('includes/pre-footer'); ?>

		<footer class="nav-gradient">
		  	<div class="container">
		   	<div class="row">
		      	<div class="span12">
		        		<nav class="main-nav">
		          		<?php wp_nav_menu( array( 'menu' => 'footer' ) ); ?>
		        		</nav>
		      	</div> <!-- end span12 -->
		    	</div> <!-- end row -->
		 	</div> <!-- end container -->
		</footer> <!-- end footer -->

		<div class="legal-social">
		  	<div class="container">
		   	<div class="row">
		      	<div class="span5">
		        		<?php wp_nav_menu( array( 'menu' => 'legal' ) ); ?>
		        		&copy; <?php echo bloginfo('sitename'); ?> | Company Number: 7406359
		        	</div>
		      
		      	<div class="span3 offset4 socials">
		      		<?php echo do_shortcode('[smartblock id=11]'); ?>
		      	</div>
		    	</div> <!-- end row -->
		  	</div> <!-- end container -->
		</div> <!-- end legal-social -->

		<script src="<?php bloginfo('stylesheet_directory'); ?>/js/bootstrap.min.js"></script> 
		<script src="<?php bloginfo('stylesheet_directory'); ?>/js/main.js"></script> 
		<!--[if (gte IE 6)&(lte IE 8)]>
		  <script type="text/javascript" src="js/selectivizr-min.js"></script>
		  <noscript><link rel="stylesheet" href="[fallback css]" /></noscript>
		<![endif]-->

		<?php wp_footer(); ?>
	</body>
</html>