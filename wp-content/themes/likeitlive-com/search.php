
<?php get_header(); ?>
	
	<div id="pre-content"><?php get_template_part('includes/pre-content'); ?></div>
			
	<div id="content">
		<div class="inner container_16">
	
			<?php if (have_posts()) : ?>
				
				<h2 class="pagetitle">Search Results for: "<?php the_search_query(); ?>"</h2>

				<?php get_template_part('includes/pagination.php'); ?>
				
					<?php while (have_posts()) : the_post(); ?>
						<div class="post post-front-page">
							<h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>            
							<div class="entry"><?php the_excerpt(); ?></div>
						</div>
					<?php endwhile; ?>

				<?php get_template_part('includes/pagination.php'); ?>

			<?php else : ?>

				<h2 class="center">Nothing was found for: "<?php the_search_query(); ?>"</h2>
				<p>Please try searching again</p>
				<?php include (TEMPLATEPATH . '/searchform.php'); ?>

			<?php endif; ?>
		
		</div>
	</div>
	
	<div id="post-content"><?php get_template_part('includes/post-content'); ?></div>
		
<?php get_footer(); ?>