<?php
	function register_custom_post_testimonials() {

		$labels = array(
			'name' => _x('Testimonials', 'post type general name'),
			'singular_name' => _x('Testimonial', 'post type singular name'),
			'add_new' => _x('Add New', 'Testimonial'),
			'add_new_item' => __('Add New Testimonial'),
			'edit_item' => __('Edit Testimonial'),
			'new_item' => __('New Testimonial'),
			'view_item' => __('View Testimonial'),
			'search_items' => __('Search Testimonials'),
			'not_found' =>  __('No Testimonials found'),
			'not_found_in_trash' => __('No Testimonials found in Trash'),
			'parent_item_colon' => ''
		);
		
		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => null,
			'supports' => array('title','editor', 'custom-fields', 'thumbnail')
		);

		register_post_type( 'testimonial' , $args );
	}
	add_action('init', 'register_custom_post_testimonials');


	if(!function_exists('get_latest_testimonial')){
		function get_latest_testimonial(){
			$args = array('post_type' => 'testimonial', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => '1');
         $testimonial = new WP_Query($args);

         if($testimonial->have_posts()):
	         while($testimonial->have_posts()): $testimonial->the_post(); ?>

					<div class="text-black source"><?php the_title(); ?><br>
					<span><?php echo get_post_meta(get_the_ID(), 'location', true); ?>, <?php the_date(get_option('date_format')); ?></span></div>
					<div class="content text-pink">&quot;<?php echo get_the_content(); ?>&quot;</div>
					<div class="text-black source"><span><?php echo get_post_meta(get_the_ID(), 'client', true); ?></span></div>

				<?php endwhile;
	     	endif;
    	}
    	add_shortcode('latest-testimonial', 'get_latest_testimonial');
   }

   