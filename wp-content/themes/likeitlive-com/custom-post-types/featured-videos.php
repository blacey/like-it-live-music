<?php
	function register_custom_post_featured_videos() {

		$labels = array(
			'name' => _x('Featured Videos', 'post type general name'),
			'singular_name' => _x('Video', 'post type singular name'),
			'add_new' => _x('Add New', 'Video'),
			'add_new_item' => __('Add New Video'),
			'edit_item' => __('Edit Video'),
			'new_item' => __('New Video'),
			'view_item' => __('View video'),
			'search_items' => __('Search videos'),
			'not_found' =>  __('No videos found'),
			'not_found_in_trash' => __('No videos found in Trash'),
			'parent_item_colon' => ''
		);
	
		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => null,
			'supports' => array('title','editor', 'custom-fields')
		);

		register_post_type( 'featured-video' , $args );
	}
	add_action('init', 'register_custom_post_featured_videos');


	

	if(!function_exists('get_featured_videos')){
		function get_featured_videos($atts){
			$num_videos = 3;

			$args = array('post_type' => 'featured-video', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => $num_videos);
         $videos = new WP_Query($args);

         if($videos->have_posts()): while($videos->have_posts()): $videos->the_post();
         	$return = '<div class="featured-item">';
            
            $return .= '<img src="' . get_bloginfo('stylesheet_directory') . '/img/featured-image.jpg">';
              
              ?> <!--<iframe src="<?php echo get_post_meta(get_the_ID(), 'video-link', true); ?>" width="223" height="151" frameborder="0"></iframe>--> <?php
            
            $return .= '<h4 class="text-pink bold"><?php the_title(); ?></h4>';
            $return .= '<p class="text-grey">' . get_the_content() . '<a href="#" class="button text-grey">More</a></p>';
            $return .= '</div>';

			endwhile; endif; 

			return $return;
    	}
    	add_shortcode('get-featured-videos', 'get_featured_videos');
   }
	