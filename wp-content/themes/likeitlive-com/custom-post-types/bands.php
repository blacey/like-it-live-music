<?php
	function register_custom_post_bands() {
		$args = array(
			'labels' => array('name' => _x('Bands', 'post type general name'),
		  				     'singular_name' => _x('Band', 'post type singular name'),
							  'add_new' => _x('Add New', 'Band'),
							  'add_new_item' => __('Add New band item'),
							  'edit_item' => __('Edit Band item'),
							  'new_item' => __('New Band Item'),
							  'view_item' => __('View Band'),
							  'search_items' => __('Search band'),
							  'not_found' =>  __('Nothing found'),
							  'not_found_in_trash' => __('Nothing found in Trash'),
							  'parent_item_colon' => ''
			),
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array('slug' => 'band', 'with_front' => true),
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => null,
			'supports' => array('title', 'editor', 'thumbnail')
		);
		register_post_type('band', $args);
	}
	add_action('init', 'register_custom_post_bands');



	function add_regions_taxonomy(){
		register_taxonomy('regions', array('band'), array("hierarchical"=>true, "label"=>"Regions", "singular_label"=>"Region", "rewrite"=>"false"));

		// force verbose rules.. this makes every Page have its own rule instead of being a 
		// catch-all, which we're going to use for the forum taxo instead
		global $wp_rewrite;
		$wp_rewrite->use_verbose_page_rules = true;

		// two rules to handle feeds
		add_rewrite_rule('(.+)/feed/(feed|rdf|rss|rss2|atom)/?$','index.php?regions=$matches[1]&feed=$matches[2]');
		add_rewrite_rule('(.+)/(feed|rdf|rss|rss2|atom)/?$','index.php?regions=$matches[1]&feed=$matches[2]');

		 // one rule to handle paging of posts in the taxo
		 add_rewrite_rule('(.+)/page/?([0-9]{1,})/?$','index.php?regions=$matches[1]&paged=$matches[2]');

		 // one rule to show the genre taxonomy normally
		 add_rewrite_rule('(.+)/?$', 'index.php?regions=$matches[1]');
	}
	add_action('init', 'add_regions_taxonomy');



	// Register the band taxonomy for Genre
	function add_genre_taxonomy(){
		register_taxonomy('genre', array('band'), array("hierarchical"=>true, "label"=>"Genres", "singular_label"=>"Genre", "rewrite"=>"false"));

		// force verbose rules.. this makes every Page have its own rule instead of being a 
		// catch-all, which we're going to use for the forum taxo instead
		global $wp_rewrite;
		$wp_rewrite->use_verbose_page_rules = true;

		// two rules to handle feeds
		add_rewrite_rule('(.+)/feed/(feed|rdf|rss|rss2|atom)/?$','index.php?genre=$matches[1]&feed=$matches[2]');
		add_rewrite_rule('(.+)/(feed|rdf|rss|rss2|atom)/?$','index.php?genre=$matches[1]&feed=$matches[2]');

		 // one rule to handle paging of posts in the taxo
		 add_rewrite_rule('(.+)/page/?([0-9]{1,})/?$','index.php?genre=$matches[1]&paged=$matches[2]');

		 // one rule to show the genre taxonomy normally
		 add_rewrite_rule('(.+)/?$', 'index.php?genre=$matches[1]');
	}
	add_action('init', 'add_genre_taxonomy');



	if(!function_exists('filter_regions_taxonomy')){
		function filter_regions_taxonomy($atts){
			extract(shortcode_atts(array(
				'hideempty' => 0,
				'orderby' => 'count',
				'order' => 'DESC'
			), $atts));

			$regions = get_terms('regions', array(
				'hide_empty' => $hideempty,
				'orderby'    => $orderby,
				'order'      => $order,
				'fields'     => 'all',
			));
			
			$return = null;
			$return .= '<ul>';
			foreach($regions as $region){
			   $return .= '<li><a href="/regions/' . $region->slug . '">' . $region->name . ' (' . $region->count . ')</a></li>';
			}
			$return .= '</ul>';
			
			return $return;
		}
		add_shortcode('filter-region', 'filter_regions_taxonomy');
	}


	if(!function_exists('filter_genre_taxonomy')){
		function filter_genre_taxonomy($atts){
			extract(shortcode_atts(array(
				'hideempty' => 1,
				'orderby' => 'count',
				'order' => 'DESC'
			), $atts));
			
			$genres = get_terms('genre', array(
				'hide_empty' => $hideempty,
				'orderby'    => $orderby,
				'order'      => $order,
				'fields'     => 'all',
			));
			
			$return = null;
			$return .= '<ul>';
			foreach($genres as $genre){
				//if('/genre/' . $genre->slug == rtrim($_SERVER['REQUEST_URI'], '/')) { $class = 'class="active_taxonomy"'; }
			   $return .= '<li><a href="/genre/' . $genre->slug . '">' . $genre->name . ' (' . $genre->count . ')</a></li>';
			}
			$return .= '</ul>';
			
			return $return;
		}
		add_shortcode('filter-genre', 'filter_genre_taxonomy');
	}



	if(!function_exists('get_bands')){
		function get_bands($atts){
			extract($atts);

			$args = array(
							'post_type' => 'band',
							'tax_query' => array(
								array(
									'taxonomy' => 'genre',
									'field' => 'id',
									'terms' => $genre,
									'include_children' => FALSE
								),

								array(
									'taxonomy' => 'regions',
									'field' => 'id',
									'terms' => $region,
									'include_children' => FALSE
								),
							),
			);
         $bands = new WP_Query($args);
         $return = null;

         if($bands->have_posts()):
         	$return .= '<div class="row">';
         		while($bands->have_posts()): $bands->the_post();
						$custom_meta = get_post_custom(get_the_ID());

						$return .= '<div class="span4">';
						$return .= '<div class="band">';
						$return .= '<h5><a href="' . get_permalink() . '">' . get_the_title() . '</a></h5>';
						
						if($custom_meta['bandratefrom'][0]):
							$return .= '<h6>FROM: ' . $custom_meta['bandratefrom'][0] . '</h6>';
						endif;

						if($custom_meta['bandprofile'][0]):
							$return .= '<p>' . substr($custom_meta['bandprofile'][0], 0, 250) . '...</p>';
						endif;
						
						$return .= '</div> <!-- end band -->';
						$return .= '</div> <!-- end span4 -->';
					endwhile;
				$return .= '</div>';
			else:
				$return .= '<p style="font-size:18px; color:#000; margin:25px 0;">No Bands to show for the Query.</p>';
	    	endif;

	    	return $return;
    	}
    	add_shortcode('getBands', 'get_bands');
   }


	function get_genres($atts){
			extract(shortcode_atts(array(
				'hideempty' => 1,
				'orderby' => 'name',
				'order' => 'DESC'
			), $atts));
			
			$genres = get_terms('genre', array(
				'hide_empty' => $hideempty,
				'orderby'    => $orderby,
				'order'      => $order,
				'fields'     => 'all'
			));
			
			$return = null;
			?>

			<?php foreach($genres as $genre): ?>
			   <div class="genre">
		   		<?php if(z_taxonomy_image_url($genre->term_id)): ?>
			   		<div class="genre-image">
			   			<a href="/genre/<?php echo $genre->slug; ?>">
			   				<img src="<?php echo z_taxonomy_image_url($genre->term_id); ?>" alt="<?php echo $genre->name; ?>" class="genre" />
			   			</a>
			   		</div>
			   	<?php endif; ?>

			   	<a href="/genre/<?php echo $genre->slug; ?>">
				   	<?php echo $genre->name; ?>
				   </a>
				</div>
			<?php endforeach;
			return $return;
		}
		add_shortcode('get-genres', 'get_genres');


   function custom_admin_css(){ ?>
   	<style type='text/css'>
   		.wp_themeSkin iframe { background:#FFF !important; }
   	</style>
   <?php }
   add_action('admin_head', 'custom_admin_css');

   add_action( 'add_meta_boxes', 'cd_meta_box_add' );

   
	function cd_meta_box_add(){  
	   add_meta_box( 'band-details', 'Band Details', 'band_details_metabox_cb', 'band', 'normal', 'high' );
	   add_meta_box( 'band-tabs', 'Band Profile', 'band_profile_metabox_cb', 'band', 'normal', 'high' );
	   add_meta_box( 'band-slider', 'Band Slider', 'band_slider_metabox_cb', 'band', 'normal', 'high' );  
	}


	function band_slider_metabox_cb($post){ 
		$values = get_post_custom( $post->ID );
		$bandslider = isset( $values['bandslider'] ) ? esc_attr( $values['bandslider'][0] ) : '';
	?>
		<p>Select the slider you want to show at the top of the band list page.</p>
		
		<select name="bandslider" class="widefat">
			<?php
				$args = array(
					'post_type' => 'sliders',
					'taxonomy'=>'slider-category'
				);
				$categories = get_categories($args);

				echo '<option value="0">Select a Slider</option>';

				foreach($categories as $category) {
					echo '<option value="' . $category->term_id . '"';
					if($bandslider == $category->term_id):
						echo ' selected';
					endif;
					echo '>' . $category->name . '</option>';
				}
			?>
		</select>
	<?php }


	function band_profile_metabox_cb($post){
		$values = get_post_custom( $post->ID );
		$bandprofile = isset( $values['bandprofile'] ) ? $values['bandprofile'][0] : '';
		$specialities = isset( $values['specialities'] ) ? $values['specialities'][0] : '';
		$lineup = isset( $values['lineup'] ) ? $values['lineup'][0] : '';
		$whychoose = isset( $values['whychoose'] ) ? $values['whychoose'][0] : '';
		$testimonials = isset( $values['testimonials'] ) ? $values['testimonials'][0] : '';
		$repertoire = isset( $values['repertoire'] ) ? $values['repertoire'][0] : '';
	?>
		<div style="height:30px; display:block; width:100%;"></div>
		<label for="bandprofile" style="font-size:1.3em; font-weight:bold;float:left;">Band Synopsis</label>
		<?php 
			$settings = array(
		    'textarea_name' => 'bandprofile',
		    'textarea_rows' => '5',
		    'media_buttons' => false,
		    'tinymce' => array(
		        'theme_advanced_buttons1' => 'bold,italic,strikethrough,underline,|,' .
		            'bullist,numlist,blockquote,|,justifyleft,justifycenter' .
		            ',justifyright,|,link,unlink,|' .
		            ',spellchecker,wp_fullscreen,wp_adv'
			    )
			);
			wp_editor($bandprofile, 'bandprofile', $settings );
		?>

		<div style="height:30px; display:block; width:100%;"></div>
		<label for="specialities" style="font-size:1.3em; font-weight:bold;float:left;">Specialities</label>
		<?php 
			$settings = array(
		    'textarea_name' => 'specialities',
		    'textarea_rows' => '10',
		    'media_buttons' => false,
		    'tinymce' => array(
		        'theme_advanced_buttons1' => 'bold,italic,strikethrough,underline,|,' .
		            'bullist,numlist,blockquote,|,justifyleft,justifycenter' .
		            ',justifyright,|,link,unlink,|' .
		            ',spellchecker,wp_fullscreen,wp_adv'
			    )
			);
			wp_editor($specialities, 'specialities', $settings );
		?>
		
		<div style="height:30px; display:block; width:100%;"></div>
		<label for="lineup" style="font-size:1.3em; font-weight:bold;float:left;">Lineup</label>
		<?php
			$settings = array(
		    'textarea_name' => 'lineup',
		    'textarea_rows' => '15',
		    'media_buttons' => false,
		    'tinymce' => array(
		        'theme_advanced_buttons1' => 'bold,italic,strikethrough,underline,|,' .
		            'bullist,numlist,blockquote,|,justifyleft,justifycenter' .
		            ',justifyright,|,link,unlink,|' .
		            ',spellchecker,wp_fullscreen,wp_adv'
			    )
			);
			wp_editor($lineup, 'lineup', $settings );
		?>

		<div style="height:30px; display:block; width:100%;"></div>
		<label for="whychoose" style="font-size:1.3em; font-weight:bold;float:left;">Why Choose</label>
		<?php 
			$settings = array(
		    'textarea_name' => 'whychoose',
		    'textarea_rows' => '5',
		    'media_buttons' => false,
		    'tinymce' => array(
		        'theme_advanced_buttons1' => 'bold,italic,strikethrough,underline,|,' .
		            'bullist,numlist,blockquote,|,justifyleft,justifycenter' .
		            ',justifyright,|,link,unlink,|' .
		            ',spellchecker,wp_fullscreen,wp_adv'
			    )
			);
			wp_editor($whychoose, 'whychoose', $settings );
		?>

		<div style="height:30px; display:block; width:100%;"></div>
		<label for="testimonials" style="font-size:1.3em; font-weight:bold;float:left;">Testimonials</label>
		<?php 
			$settings = array(
		    'textarea_name' => 'testimonials',
		    'textarea_rows' => '20',
		    'media_buttons' => false,
		    'tinymce' => array(
		        'theme_advanced_buttons1' => 'bold,italic,strikethrough,underline,|,' .
		            'bullist,numlist,blockquote,|,justifyleft,justifycenter' .
		            ',justifyright,|,link,unlink,|' .
		            ',spellchecker,wp_fullscreen,wp_adv'
			    )
			);
			wp_editor($testimonials, 'testimonials', $settings );
		?>

		<div style="height:30px; display:block; width:100%;"></div>
		<label for="repertoire" style="font-size:1.3em; font-weight:bold;float:left;">Repertoire</label>
		<?php 
			$settings = array(
		    'textarea_name' => 'repertoire',
		    'textarea_rows' => '20',
		    'media_buttons' => false,
		    'tinymce' => array(
		        'theme_advanced_buttons1' => 'bold,italic,strikethrough,underline,|,' .
		            'bullist,numlist,blockquote,|,justifyleft,justifycenter' .
		            ',justifyright,|,link,unlink,|' .
		            ',spellchecker,wp_fullscreen,wp_adv'
			    )
			);
			wp_editor($repertoire, 'repertoire', $settings );
		?>

	<?php }  // end band_profile_metabox_cb





	function band_details_metabox_cb($post){
		$values = get_post_custom( $post->ID );  
		$bandratefrom = isset( $values['bandratefrom'] ) ? esc_attr( $values['bandratefrom'][0] ) : '';
		$bandrateto = isset( $values['bandrateto'] ) ? esc_attr( $values['bandrateto'][0] ) : ''; 
		$videolink = isset( $values['videolink'] ) ? esc_attr( $values['videolink'][0] ) : '';
		//$selected = isset( $values['my_meta_box_select'] ) ? esc_attr( $values['my_meta_box_select'][0] ) : '';  
		//$check = isset( $values['my_meta_box_check'] ) ? esc_attr( $values['my_meta_box_check'][0] ) : '';
	?>	
		<p class="wrapper">
			<label for="bandratefrom">Band Rate From:</label>
			<input type="text" name="bandratefrom" id="bandratefrom" value="<?php echo $bandratefrom; ?>" style="width:100%" />  
		</p>

		<p class="wrapper">
			<label for="bandratefrom">Band Rate To:</label>
	    	<input type="text" name="bandrateto" id="bandrateto" value="<?php echo $bandrateto; ?>"  style="width:100%"/>
	   </p>

	   <p class="wrapper">
	   	<label for="videolink">YouTube Video:</label>
	   	<input type="text" name="videolink" id="videolink" value="<?php echo $videolink; ?>"  style="width:100%"/>
	   </p>
	<?php } // end cb_meta_box_cb



	add_action( 'save_post', 'band_metabox_save' );
	function band_metabox_save( $post_id ){
		//echo '<pre>';
		//var_dump($_POST);
		//exit();

		// Save Slider Details
		if(isset($_POST['bandslider'])){
     		update_post_meta( $post_id, 'bandslider', $_POST['bandslider'] );  
		}

		// Save Band Details
	   if(isset($_POST['bandratefrom'])){
     		update_post_meta( $post_id, 'bandratefrom', $_POST['bandratefrom'] );  
		}

		if(isset($_POST['bandrateto'])){
     		update_post_meta( $post_id, 'bandrateto', $_POST['bandrateto'] );  
		}

		if(isset($_POST['videolink'])){
     		update_post_meta( $post_id, 'videolink', $_POST['videolink'] );  
		}


		// Save Band Tabs
		if(isset($_POST['bandprofile'])){
     		update_post_meta( $post_id, 'bandprofile', $_POST['bandprofile'] );  
		}

		if(isset($_POST['specialities'])){
     		update_post_meta( $post_id, 'specialities', $_POST['specialities'] );  
		}

		if(isset($_POST['lineup'])){
     		update_post_meta( $post_id, 'lineup', $_POST['lineup'] );  
		}

		if(isset($_POST['whychoose'])){
     		update_post_meta( $post_id, 'whychoose', $_POST['whychoose'] );  
		}

		if(isset($_POST['testimonials'])){
     		update_post_meta( $post_id, 'testimonials', $_POST['testimonials'] );  
		}

		if(isset($_POST['repertoire'])){
     		update_post_meta( $post_id, 'repertoire', $_POST['repertoire'] );  
		}
	}

