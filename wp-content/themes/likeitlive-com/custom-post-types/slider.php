<?php
	function register_post_type_band_sliders() {
		$args = array(
			'labels' => array('name' => _x('Band Sliders', 'post type general name'),
		  				     'singular_name' => _x('Slide', 'post type singular name'),
							  'add_new' => _x('Add New Slide', 'Photo'),
							  'add_new_item' => __('Add New Slide'),
							  'edit_item' => __('Edit Slide'),
							  'new_item' => __('New Slide'),
							  'view_item' => __('View Slide'),
							  'search_items' => __('Search Slides'),
							  'not_found' =>  __('Nothing found'),
							  'not_found_in_trash' => __('Nothing found in Trash'),
							  'parent_item_colon' => ''
			),
			'public' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => null,
			'supports' => array('title', 'editor', 'thumbnail')
		);
		register_post_type('sliders', $args);
	}
	add_action('init', 'register_post_type_band_sliders');

	function add_slider_taxonomy(){
		register_taxonomy('slider-category', array('sliders'), array("hierarchical"=>true, "label"=>"Slide Category", "singular_label"=>"Slide Category", "rewrite"=>"false"));
	}
	add_action('init', 'add_slider_taxonomy');