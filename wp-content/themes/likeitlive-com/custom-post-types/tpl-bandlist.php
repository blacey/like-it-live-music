<?php /* Template Name: Band List */ ?>
<?php get_header(); ?>

	<div id="wrap-content">

		<div class="container">
		  	<?php get_template_part('includes/pre-content'); ?>
		</div>

		<div id="content">
			<div class="inner">
				<div class="container bg-white bands-content">
					<div class="row">
						<div class="span3 bands-filters">
							<?php get_template_part('includes/left-sidebar'); ?>
						</div>

						<div class="span9 bands-list">
							<div class="row first-pagination-row bg-none">
								<div class="span6">
									<h2>View all our bands</h2>
								</div>

								<?php get_template_part('includes/pagination'); ?>
							</div>

							<?php
								$args = array('post_type' => 'band');
								$query = new WP_Query($args);

								if($query->have_posts()):
									while ($query->have_posts()) : $query->the_post();
										get_template_part('includes/band-list');
									endwhile;
								endif;
							?>
						</div> <!-- end span9 bands-list -->
					</div>
				</div>
			</div>
		</div>	
		
		<div id="post-content"><?php get_template_part('includes/post-content'); ?></div>
	
	</div>

<?php get_footer(); ?>