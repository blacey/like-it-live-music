<h1 class="page-title"><?php single_cat_title(); ?></h1>
<?php if ( category_description() ) : ?>
  <div class="category-description"><?php echo category_description(); ?></div>
<?php endif; ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header"><h2 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2></header>  
        <div class="entry-content"><?php the_excerpt(); ?></div>
      </article>
<?php endwhile; else : ?>
  <?php get_template_part('loop-error', '404'); ?>  
<?php endif; ?>