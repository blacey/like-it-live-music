<?php
	/* 
		Template Name: SEO Page
	*/
?>

<?php get_header(); ?>
	
	<div class="container">
	  	<?php get_template_part('includes/pre-content'); ?>
	</div>

	<div class="container bg-white landing-page">
		<div class="row">
			<div class="span9 bands-list">
				<?php while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
			</div>

			<div class="span3 bands-filters">
				<?php get_template_part('includes/right-sidebar'); ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
