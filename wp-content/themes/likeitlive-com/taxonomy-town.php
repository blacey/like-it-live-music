<?php get_header(); ?>
	
	<div class="container">
	  	<?php get_template_part('includes/pre-content'); ?>
	</div>

	<div class="container bg-white bands-content">
		<div class="row">
			<div class="span3 bands-filters">
				<?php get_template_part('includes/left-sidebar'); ?>
			</div>

			<div class="span9 bands-list">
				<h2><?php echo $term->name; ?></h2>

				<?php while (have_posts()) : the_post(); ?>
					<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
					<?php the_excerpt(); ?>
				<?php endwhile; ?>
			</div>

			<div class="row last-pagination-row bg-none">
				<div class="span6"></div>
				<?php get_template_part('includes/pagination'); ?>
			</div>

		</div>
	</div>

<?php get_footer(); ?>
