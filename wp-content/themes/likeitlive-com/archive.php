<?php get_header(); ?>
	<div id="pre-content"><?php get_template_part('includes/pre-content'); ?></div>			
	<div id="content">
		<div class="inner">						
			<div id="posts">
				<?php if (have_posts()) : ?>
					<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
					<?php if (is_category()) : ?>
						<h1 class="page-title">Category Archive&#8216;<?php single_cat_title(); ?>&#8217;</h1>
					<?php elseif ( is_tag() ) : ?>
						<h1 class="page-title">Posts tagged under &#8216;<?php single_tag_title(); ?>&#8217;</h1>
					<?php elseif (is_day()) : ?>
						<h1 class="page-title">Date Archive <?php the_time('d/m/Y'); ?></h1>
					<?php elseif (is_month()) : ?>
						<h1 class="page-title">Date Archive <?php the_time('M Y'); ?></h1>
					<?php elseif (is_year()) : ?>
						<h1 class="page-title">Daye Archive <?php the_time('Y'); ?></h1>
					<?php elseif (is_author()) : ?>
						<h1 class="page-title">Archive</h1>
					<?php elseif (isset($_GET['paged']) && !empty($_GET['paged'])) : ?>
						<h1 class="page-title">Archivio Blog</h1>
					<?php endif; ?>
					<?php while (have_posts()) : the_post(); ?>
						<div class="post post-front-page">
							<h1><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
							<div class="entry"><?php the_excerpt(); ?></div>
						</div>        
					<?php endwhile; ?>
					<div class="navigation">
						<?php if(function_exists('wp_pagenavi')) : wp_pagenavi(); else : ?>
						<div class="alignleft"><?php next_posts_link('&laquo; Older posts') ?></div>
						<div class="alignright"><?php previous_posts_link('Newer posts &raquo;') ?></div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>	
		</div>
	</div>	
	<div id="post-content"><?php get_template_part('includes/post-content'); ?></div>	
<?php get_footer(); ?>
