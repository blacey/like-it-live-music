jQuery(document).ready(function(e) {
	
    jQuery('.mobile-nav-icon').click(function(e) {
        jQuery('.mobile-nav').slideToggle();
    });
	
	 jQuery('.tags a:first').tab('show');
	 
	 var modalHtml = '<div class="modal-header"><button class="close">&times;</button><h3>Modal header</h3></div><div class="modal-body"><p class="video-holder"></p></div><div class="modal-footer"> <a href="#" class="btn close">Close</a></div>';
	 var video = '<iframe width="420" height="315" src="http://www.youtube.com/embed/gbViIZR0YS8" frameborder="0" allowfullscreen></iframe>';
	 jQuery('.band-samples img').click(function(e) {
		 
        jQuery('#player-modal').append(modalHtml);
		jQuery('.video-holder').append(video);
		jQuery('#player-modal').modal();
    });
	jQuery('#player-modal').on('click',function() {
		jQuery('#player-modal').modal('hide').empty();	
	});
});