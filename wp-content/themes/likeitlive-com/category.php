<?php get_header(); ?>	
	<div id="pre-content"><?php get_template_part('includes/pre-content'); ?></div>			
	<div id="content">
		<div class="inner">		
			<div id="posts">
        <?php get_template_part( 'loop-category', get_category( get_query_var('cat') )->slug ); ?>
      </div>
		</div>
	</div>	
	<div id="post-content"><?php get_template_part('includes/post-content'); ?></div>	
<?php get_footer(); ?> 