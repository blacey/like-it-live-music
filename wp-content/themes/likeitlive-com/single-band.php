<?php get_header(); ?>

<div class="container band-details">
  	<?php get_template_part('includes/pre-content'); ?>
</div>

<div class="container">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<?php $custom_meta = get_post_custom(get_the_ID()); ?>
		
	  	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="container">
				<div class="row">
					<div class="span9 band-profile">
					
					  <ul class="tags nav nav-tabs">
				         <?php if(get_the_content()): ?><li class="tag"><a href="#tag-text1" data-toggle="tab">Band profile</a></li><?php endif; ?>
				         <?php if($custom_meta['specialities'][0]): ?><li class="tag"><a href="#tag-text2" data-toggle="tab">Specialities &amp; genres</a></li><?php endif; ?>
				         <?php if($custom_meta['lineup'][0]): ?><li class="tag"><a href="#tag-text3" data-toggle="tab">line-up</a></li><?php endif; ?>
				         <?php if($custom_meta['whychoose'][0]): ?><li class="tag"><a href="#tag-text4" data-toggle="tab">Why choose</a></li><?php endif; ?>
				         <?php if($custom_meta['testimonials'][0]): ?><li class="tag"><a href="#tag-text5" data-toggle="tab">Testimonials</a></li><?php endif; ?>
				         <?php if($custom_meta['repertoire'][0]): ?><li class="tag"><a href="#tag-text6" data-toggle="tab">Repertoire</a></li><?php endif; ?>
				      </ul>

				      <div class="tab-content">
				         <?php if(get_the_content()): ?>
					         <div class="tab-pane" id="tag-text1"><?php the_content(); ?></div>
					      <?php endif; ?>

				         <?php if($custom_meta['specialities'][0]): ?>
					         <div class="tab-pane" id="tag-text2"><?php echo nl2br($custom_meta['specialities'][0]); ?></div>
					      <?php endif; ?>

				         <?php if($custom_meta['lineup'][0]): ?>
					         <div class="tab-pane" id="tag-text3"><?php echo nl2br($custom_meta['lineup'][0]); ?></div>
					      <?php endif; ?>

				         <?php if($custom_meta['whychoose'][0]): ?>
					         <div class="tab-pane" id="tag-text4"><?php echo nl2br($custom_meta['whychoose'][0]) ?></div>
					      <?php endif; ?>

				         <?php if($custom_meta['testimonials'][0]): ?>
					         <div class="tab-pane" id="tag-text5"><?php echo nl2br($custom_meta['testimonials'][0]); ?></div>
					      <?php endif; ?>

					      <?php if($custom_meta['repertoire'][0]): ?>
					      	<div class="tab-pane" id="tag-text6"><?php echo nl2br($custom_meta['repertoire'][0]); ?></div>
						   <?php endif; ?>
				      </div>

				   	<?php echo do_shortcode('[smartblock id=82]'); ?>
				   </div> <!-- end band-profile -->


				   <div class="span3 band-samples">
				   	<div class="container">
					   	<div class="row">
					   		<div class="span6">
							   	<?php if($custom_meta['videolink'][0]): ?>
								   	<h3>FEATURED VIDEO</h3>
								   	<iframe src="<?php echo $custom_meta['videolink'][0]; ?>" width="262" height="168" frameborder="0" allowfullscreen></iframe>
								   <?php endif; ?>
								</div> <!-- end span6 -->
								
							   <div class="span6">
							   	<?php $audio = get_children( array('post_parent' => $post->ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'audio' ) ); ?>
									<?php if($audio): ?>
								   	<h3>SAMPLE SONG LIST</h3>
										<ul>
										   <?php foreach ( $audio as $attachment_id => $attachment ) : ?>
										    	<li>
										    		<a href="<?php echo wp_get_attachment_url( $attachment_id, 'full' ); ?>" title="<?php echo wp_get_attachment_link( $attachment_id, '' , false, false, ' '); ?>" rel="audio-file"><?php echo wp_get_attachment_link( $attachment_id, '' , false, false, ' '); ?></a>
										    	</li>
										  	<?php endforeach; ?>
										</ul>
									<?php endif; ?>
								</div> <!-- end span6 -->
							</div> <!-- end row -->
						</div> <!-- end container -->

				   </div> <!-- end band-samples -->
				</div> <!-- end row -->
	   	</div> <!-- end container -->

			<?php endwhile; else : ?>		
			  <?php get_template_part('loop-error', '404'); ?>  
			<?php endif; ?>
		</article>
	</div> <!-- end container -->


<?php get_template_part('includes/post-content'); ?>

<?php get_footer(); ?>