<?php 
	/* Template Name: Testimonials */
?>

<?php get_header(); ?>
	<div class="container testimonials-page">
	  	<?php get_template_part('includes/pre-content'); ?>
	
		<div class="testimonials-wrapper">
			<?php if(have_posts()): ?>
				<?php while(have_posts()): the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
			<?php endif; wp_reset_query(); ?>

			<?php
				$args = array('post_type' => 'testimonial', 'orderby' => 'date', 'order' => 'DESC', 'posts_per_page' => get_option('posts_per_page'));
	         $testimonials = new WP_Query($args);

	         if($testimonials->have_posts()): ?>
	            <?php while($testimonials->have_posts()): $testimonials->the_post(); ?>
						<div class="row testimonial-holder has-shadow">
							<div class="span8">
								<p class="testimonial-quote"><?php echo get_the_content(); ?></p>

								<p class="testimonial-source"><?php echo get_the_title(); ?></p>
							</div> <!-- end span8 -->

							<div class="span3 band">
								<?php 
									if(has_post_thumbnail()):
										the_post_thumbnail();
									endif;
								?>
								<h5>BAND NAME HERE</h5>
								<h6>BAND PRICE</h6>
							</div>
						</div>
					<?php endwhile; ?>
		      <?php endif;
		   ?>
		</div>
	</div> <!-- end container testimonials-page -->

<?php get_footer(); ?>