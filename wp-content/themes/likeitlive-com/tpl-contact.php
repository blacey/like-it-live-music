<?php 
	/* Template Name: Contact Form */
?>

<?php get_header(); ?>
	
	<div class="container">
	  	<?php get_template_part('includes/pre-content'); ?>
	</div>

	<div class="full-width contact container bg-white">
		<div class="row">
			<div class="span12">
				<?php the_content(); ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
