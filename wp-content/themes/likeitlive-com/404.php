<?php get_header(); ?>
	
	<div class="container">
	  	<?php get_template_part('includes/pre-content'); ?>
	</div>

	<div class="container bg-white">
		<div class="row">
			<div class="span3 bands-filters">
				<?php get_template_part('includes/left-sidebar'); ?>
			</div>

			<div class="span9 bands-list">
				  <div class="entry-content">
				  		<h2>We're sorry but we can't find the page you requested</h2>
				    	<p>If you beleive this is in error please <a href="/contact-us" title="Contact Us">contact us</a>. In the meantime why don't you stay and checkout some of our <a href="/bands" title="Bands">bands</a>?</p>
				  </div>
			</div>	
		</div>
	</div>

<?php get_footer(); ?>
