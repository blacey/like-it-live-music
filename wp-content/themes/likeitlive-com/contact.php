<?php 
	/* Template Name: Contact Us */
?>

<?php get_header(); ?>
	
	<div class="container">
	  	<?php get_template_part('includes/pre-content'); ?>
	</div>

	<div class="full-width container bg-white">
		<div class="row">
			<div class="span12 contact">
				<?php the_content(); ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>