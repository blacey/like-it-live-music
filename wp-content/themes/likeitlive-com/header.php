<!DOCTYPE html>
<html dir="ltr" lang="en">
	<head>
		<title><?php wp_title(''); ?></title>
		

		<meta charset="iso-8859-7">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/bootstrap.css?v=<?php echo rand('1', '100'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/bootstrap-responsive.css?v=<?php echo rand('1', '100'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/likeitlive.css?v=<?php echo rand('1', '100'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/likeitlive-responsive.css?v=<?php echo rand('1', '100'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/style.css?v=<?php echo rand('1', '100'); ?>">
		<!--[if lt IE 9]>
			<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		<!--[if IE 9]>
			<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/ie9.css">
		<![endif]-->

		<?php if(is_single()): ?>
			<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
			<link rel="alternate" type="text/xml" title="Comments RSS 2.0 feed" href="<?php bloginfo('comments_rss2_url'); ?>" />
		<?php endif; ?>
		
		<?php wp_head(); ?>
		<?php wp_enqueue_script('jquery'); ?>
	</head>
	
	<body <?php body_class(); ?>>
		<header>
	   	<?php if(is_active_sidebar('pre-header')): ?>
	   		<div class="pre-nav">
	   			<?php dynamic_sidebar('pre-header'); ?>
	   		</div>
		   <?php endif; ?>

		  	<div class="header-colorful-line">
		   	<div class="nav-gradient">
		      	<div class="container">
		        		<div class="row">
		        			<a href="#" class="visible-phone mobile-nav-icon"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/mobile_nav_icon.png"></a>
		         		<div class="logo span3"><a href="/"><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/likeitlive-logo.png"></a></div>
		         		<nav class="main-nav span9 hidden-phone">
		            		<?php wp_nav_menu( array( 'menu' => 'primary', 'menu_class' => 'nav' ) ); ?>
		         		</nav>
		         	
		         		<nav class="span9 mobile-nav">
		         			<?php wp_nav_menu( array( 'menu' => 'primary' ) ); ?>
		         		</nav>
		        		</div>
		      	</div>
		    	</div>
		  	</div>
		</header>

		<div class="main">