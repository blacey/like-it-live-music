<?php get_header(); ?>
	
	<div class="container">
	  	<?php get_template_part('includes/pre-content'); ?>
	</div>

	<div class="container bg-white bands-content">
		<div class="row">
			<div class="span3 bands-filters">
				<?php get_template_part('includes/left-sidebar'); ?>
			</div>

			<div class="span9 bands-list">
				<?php $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy')); ?>
				
				<div class="row first-pagination-row bg-none">
					<div class="span6">
						<h2><?php echo $term->name; ?></h2>
					</div>

					<?php get_template_part('includes/pagination'); ?>
				</div>
					
				<?php while (have_posts()) : the_post(); ?>
					<?php get_template_part('includes/band-list'); ?>
				<?php endwhile; ?>
				
				<div class="row last-pagination-row bg-none">
					<div class="span6"></div>
					<?php get_template_part('includes/pagination'); ?>
				</div>

			</div>	
		</div>
	</div>

<?php get_footer(); ?>
