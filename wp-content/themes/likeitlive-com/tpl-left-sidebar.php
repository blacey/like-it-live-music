<?php /* Template Name: Left Sidebar */ ?>
<?php get_header(); ?>
	<div id="wrap-content">
		<div id="pre-content"><?php get_template_part('includes/pre-content'); ?></div>
		<div id="content">
			<div class="inner">
				<div class="<?php left_sidebar_container(); ?>">
					<div id="left-sidebar" class="<?php left_sidebar_grid(1); ?> aside_wrap">
						<?php get_template_part('includes/left-sidebar'); ?>
					</div>				
					
					<div class="<?php left_sidebar_grid(2); ?>">
						<div id="posts">
							<?php get_template_part('loop-single'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<div id="post-content"><?php get_template_part('includes/post-content'); ?></div>
	</div>
<?php get_footer(); ?>