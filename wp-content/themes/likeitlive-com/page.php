<?php get_header(); ?>
	
	<div class="container">
	  	<?php get_template_part('includes/pre-content'); ?>
	</div>

	<div class="container bg-white">
		<div class="row">
			<div class="span3 bands-filters">
				<?php get_template_part('includes/left-sidebar'); ?>
			</div>

			<div class="span9 bands-list">
				<?php the_content(); ?>
			</div>	
		</div>
	</div>

<?php get_footer(); ?>
