<?php /* Template Name: Right Sidebar */ ?>
<?php get_header(); ?>
	<div id="wrap-content">
		<div id="pre-content"><?php get_template_part('includes/pre-content'); ?></div>			
		<div id="content">
		  <div class="inner">
			<div class="<?php right_sidebar_container(); ?>">
			  <div class="<?php right_sidebar_grid(1); ?>">
				<div id="posts">
				  <?php get_template_part('loop-single'); ?>
				</div>
			  </div>
			  <div class="<?php right_sidebar_grid(2); ?>">
				<div id="right-sidebar" class="sidebar-inner"><?php get_template_part('includes/right-sidebar'); ?></div>
			  </div>
			</div>
		  </div>
		</div>	
		<div id="post-content"><?php get_template_part('includes/post-content'); ?></div>	
	</div>
<?php get_footer(); ?>